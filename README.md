This is a bunch of uncommented code I wrote for visualizing 
[DW-NOMINATE](https://voteview.com/about)
data.  I used it to generate images like the following:

  https://static.aperiodic.net/dw-nominate/house.png

The DW-NOMINATE data is from:

  http://k7moa.com/dwnomin.htm

You'll need Python (3), pandas, numpy, and matplotlib.  If you want to
make videos, you'll also need cairo and something to join the frames
together.  (I used ffmpeg for the last part.)

To do things with the data, run `get-data` to grab the data files.
`make-image.py` is the main image generation program.

`movie.py` will make a series of images in the (hardcoded; sorry) `/tmp/v`
directory.  I joined them together into a video with:

    ffmpeg -y -r 30 -f image2 -i /tmp/v/%05d.png -vcodec libx264 -crf 25 -pix_fmt yuv420p /tmp/video.mp4

