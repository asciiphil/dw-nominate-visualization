#!/usr/bin/env python3

import datetime
import math
import subprocess

import cairo
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import pandas as pd

# Borrowed from ColorBrewer
COLORS = pd.DataFrame({
    'red': (228,26,28),
    'blue': (55,126,184),
    'green': (77,175,74),
    'purple': (152,78,163),
    'orange': (255,127,0),
    'yellow': (255,255,51),
    'brown': (166,86,40),
    'pink': (247,129,191),
    'grey': (153,153,153),
}) / 255

PARTY_NAMES = pd.Series({
    1: 'Federalist',
    9: 'Jefferson Republican',
    10: 'Anti-Federalist',
    11: 'Jefferson Democrat',
    13: 'Democrat-Republican',
    22: 'Adams',
    25: 'National Republican',
    26: 'Anti Masonic',
    29: 'Whig',
    34: 'Whig and Democrat',
    37: 'Constitutional Unionist',
    40: 'Anti-Democrat and States Rights',
    41: 'Anti-Jackson Democrat',
    43: 'Calhoun Nullifier',
    44: 'Nullifier',
    46: 'States Rights',
    48: 'States Rights Whig',
    100: 'Democratic',
    101: 'Jackson Democrat',
    103: 'Democrat and Anti-Mason',
    104: 'Van Buren Democrat',
    105: 'Conservative Democrat',
    108: 'Anti-Lecompton Democrat',
    110: 'Popular Sovereignty Democrat',
    112: 'Conservative',
    114: 'Readjuster',
    117: 'Readjuster Democrat',
    118: 'Tariff for Revenue Democrat',
    119: 'United Democrat',
    200: 'Republican',
    202: 'Union Conservative',
    203: 'Unconditional Unionist',
    206: 'Unionist',
    208: 'Liberal Republican',
    212: 'United Republican',
    213: 'Progressive Republican',
    214: 'Non-Partisan and Republican',
    215: 'War Democrat',
    300: 'Free Soil',
    301: 'Free Soil Democrat',
    302: 'Free Soil Whig',
    304: 'Anti-Slavery',
    308: 'Free Soil American and Democrat',
    310: 'American',
    326: 'National Greenbacker',
    328: 'Independent',
    329: 'Ind. Democrat',
    331: 'Ind. Republican',
    333: 'Ind. Republican-Democrat',
    336: 'Anti-Monopolist',
    337: 'Anti-Monopoly Democrat',
    340: 'Populist',
    341: 'People\'s',
    347: 'Prohibitionist',
    353: 'Ind. Silver Republican',
    354: 'Silver Republican',
    355: 'Union',
    356: 'Union Labor',
    370: 'Progressive',
    380: 'Socialist',
    401: 'Fusionist',
    402: 'Liberal',
    403: 'Law and Order',
    522: 'American Labor',
    523: 'American Labor (La Guardia)',
    537: 'Farmer-Labor',
    555: 'Jackson',
    603: 'Ind. Whig',
    1060: 'Silver',
    1061: 'Emancipationist',
    1111: 'Liberty',
    1116: 'Conservative Republican',
    1275: 'Anti-Jackson',
    1346: 'Jackson Republican',
    3333: 'Opposition',
    3334: 'Opposition (36th Congress)',
    4000: 'Anti-Administration',
    4444: 'Union',
    5000: 'Pro-Administration',
    6000: 'Crawford Federalist',
    6666: 'Crawford Republican',
    7000: 'Jackson Federalist',
    7777: 'Crawford Republican',
    8000: 'Adams-Clay Federalist',
    8888: 'Adams-Clay Republican',
    9000: 'Unknown',
    9999: 'Unknown',
})

PARTY_COLORS = pd.Series({
     100: COLORS.blue,  # Democrat - blue
      13: COLORS.blue,  # Democrat-Republican - blue
     200: COLORS.red,  # Republican
      29: COLORS.purple,  # Whig
     555: COLORS.orange,  # Jackson
       1: COLORS.yellow,  # Federalist
    1275: COLORS.brown,  # Anti-Jackson
      22: COLORS.pink,  # Adams
    # Everyone else - grey
    5000: COLORS.grey,  # Pro-Administration
    3333: COLORS.grey,  # Opposition
    3334: COLORS.grey,  # Opposition (36th Congress)
    4000: COLORS.grey,  # Anti-Administration
    9: COLORS.grey,  # Jefferson Republican
    10: COLORS.grey,  # Anti-Federalist
    11: COLORS.grey,  # Jefferson Democrat
    25: COLORS.grey,  # National Republican
    26: COLORS.grey,  # Anti Masonic
    34: COLORS.grey,  # Whig and Democrat
    37: COLORS.grey,  # Constitutional Unionist
    40: COLORS.grey,  # Anti-Democrat and States Rights
    41: COLORS.grey,  # Anti-Jackson Democrat
    43: COLORS.grey,  # Calhoun Nullifier
    44: COLORS.grey,  # Nullifier
    46: COLORS.grey,  # States Rights
    48: COLORS.grey,  # States Rights Whig
    101: COLORS.grey,  # Jackson Democrat
    103: COLORS.grey,  # Democrat and Anti-Mason
    104: COLORS.grey,  # Van Buren Democrat
    105: COLORS.grey,  # Conservative Democrat
    108: COLORS.grey,  # Anti-Lecompton Democrat
    110: COLORS.grey,  # Popular Sovereignty Democrat
    112: COLORS.grey,  # Conservative
    114: COLORS.grey,  # Readjuster
    117: COLORS.grey,  # Readjuster Democrat
    118: COLORS.grey,  # Tariff for Revenue Democrat
    119: COLORS.grey,  # United Democrat
    202: COLORS.grey,  # Union Conservative
    203: COLORS.grey,  # Unconditional Unionist
    206: COLORS.grey,  # Unionist
    208: COLORS.grey,  # Liberal Republican
    212: COLORS.grey,  # United Republican
    213: COLORS.grey,  # Progressive Republican
    214: COLORS.grey,  # Non-Partisan and Republican
    215: COLORS.grey,  # War Democrat
    300: COLORS.grey,  # Free Soil
    301: COLORS.grey,  # Free Soil Democrat
    302: COLORS.grey,  # Free Soil Whig
    304: COLORS.grey,  # Anti-Slavery
    308: COLORS.grey,  # Free Soil American and Democrat
    310: COLORS.grey,  # American
    326: COLORS.grey,  # National Greenbacker
    328: COLORS.grey,  # Independent 
    329: COLORS.grey,  # Ind. Democrat
    331: COLORS.grey,  # Ind. Republican
    333: COLORS.grey,  # Ind. Republican-Democrat
    336: COLORS.grey,  # Anti-Monopolist
    337: COLORS.grey,  # Anti-Monopoly Democrat
    340: COLORS.grey,  # Populist
    341: COLORS.grey,  # People's
    347: COLORS.grey,  # Prohibitionist
    353: COLORS.grey,  # Ind. Silver Republican
    354: COLORS.grey,  # Silver Republican
    355: COLORS.grey,  # Union 
    356: COLORS.grey,  # Union Labor
    370: COLORS.grey,  # Progressive
    380: COLORS.grey,  # Socialist
    401: COLORS.grey,  # Fusionist
    402: COLORS.grey,  # Liberal
    403: COLORS.grey,  # Law and Order
    522: COLORS.grey,  # American Labor
    523: COLORS.grey,  # American Labor (La Guardia)
    537: COLORS.grey,  # Farmer-Labor
    603: COLORS.grey,  # Ind. Whig
    1060: COLORS.grey,  # Silver
    1061: COLORS.grey,  # Emancipationist 
    1111: COLORS.grey,  # Liberty
    1116: COLORS.grey,  # Conservative Republican
    1346: COLORS.grey,  # Jackson Republican
    4444: COLORS.grey,  # Union
    6000: COLORS.grey,  # Crawford Federalist
    6666: COLORS.grey,  # Crawford Republican
    7000: COLORS.grey,  # Jackson Federalist
    7777: COLORS.grey,  # Crawford Republican
    8000: COLORS.grey,  # Adams-Clay Federalist
    8888: COLORS.grey,  # Adams-Clay Republican     
    9000: COLORS.grey,  # Unknown
    9999: COLORS.grey,  # Unknown
})

VIDEO_FPS = 30
REST_SECONDS = 0.25
TRANSITION_SECONDS = 0.5

CANVAS_SIDE = 1024
NOMINATE_MAX = 1.5
POINT_RADIUS = 6
SCALE = CANVAS_SIDE / 2 / NOMINATE_MAX
POINT_ALPHA = 0.5

def draw_point(row, ctx):
    color = pd.concat([PARTY_COLORS[row.party_code], pd.Series([POINT_ALPHA])], axis=0)

    ctx.set_source_rgba(*color)
    ctx.arc(row.nominate_dim1, row.nominate_dim2, POINT_RADIUS / SCALE, 0, 2 * math.pi)
    ctx.fill()

def new_image():
    image_width = CANVAS_SIDE
    image_height = CANVAS_SIDE
    surface = cairo.ImageSurface(cairo.FORMAT_RGB24, image_width, image_height)
    ctx = cairo.Context(surface)
    
    ctx.rectangle(0, 0, image_width, image_height)
    ctx.set_source_rgb(1, 1, 1)
    ctx.fill()
    
    ctx.transform(cairo.Matrix(SCALE, 0,
                               0, -SCALE,
                               CANVAS_SIDE / 2, CANVAS_SIDE / 2))

    return surface, ctx

def render_congress(number, index):
    surface, ctx = new_image()
    text_ctx = cairo.Context(surface)
    
    cong_df = df[df.congress == number].set_index(['state_abbrev', 'district_code'])
    for row in cong_df.itertuples():
        draw_point(row, ctx)

    text_ctx.move_to(16, 16)
    text_ctx.show_text('{}-{} ({})'.format(
        1787 + 2 * number,
        1787 + 2 * number + 1,
        number))
    
    for i in range(0, int(VIDEO_FPS * REST_SECONDS)):
        surface.write_to_png('/tmp/v/{:05}.png'.format(index))
        index += 1

    return index

def render_transition(number, index):
    cong_df = df[df.congress == number].set_index(['state_abbrev', 'district_code'])
    next_cong_df = df[df.congress == number + 1].set_index(['state_abbrev', 'district_code'])
    common_districts = cong_df.index.intersection(next_cong_df.index)
    start_pos_array = cong_df[~cong_df.index.duplicated()].loc[common_districts][['nominate_dim1', 'nominate_dim2']].values
    end_pos_array = next_cong_df[~next_cong_df.index.duplicated()].loc[common_districts][['nominate_dim1', 'nominate_dim2']].values
    start_color_array = pd.DataFrame(list(PARTY_COLORS[cong_df[~cong_df.index.duplicated()].loc[common_districts].party_code])).values
    end_color_array = pd.DataFrame(list(PARTY_COLORS[next_cong_df[~next_cong_df.index.duplicated()].loc[common_districts].party_code])).values

    pos_delta = (end_pos_array - start_pos_array) / (VIDEO_FPS * TRANSITION_SECONDS)
    color_delta = (end_color_array - start_color_array) / (VIDEO_FPS * TRANSITION_SECONDS)
    for f in range(0, int(VIDEO_FPS * TRANSITION_SECONDS)):
        surface, ctx = new_image()
        text_ctx = cairo.Context(surface)
        text_ctx.move_to(16, 16)
        text_ctx.show_text('{}'.format(1787 + 2 * number + 1))
        trans_pos = start_pos_array + pos_delta * f
        trans_color = start_color_array + color_delta * f
        for r in range(0, trans_pos.shape[0]):
            ctx.set_source_rgba(*trans_color[r], POINT_ALPHA)
            ctx.arc(*trans_pos[r], POINT_RADIUS / SCALE, 0, 2 * math.pi)
            ctx.fill()
        surface.write_to_png('/tmp/v/{:05}.png'.format(index))
        index += 1

    return index


df = pd.read_csv('Hall_members.csv')
# Fix origin data.  "Republican" (200) before 1853 (33rd Congress)
# should be "Democratic-Republican" (13)
df.loc[(df.congress < 33) & (df.party_code == 200), 'party'] = 13

index = 0

for c in range(df.congress.min(), df.congress.max()):
    index = render_congress(c, index)
    index = render_transition(c, index)
for i in range(0, 5):
    index = render_congress(df.congress.max(), index)
